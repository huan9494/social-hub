Rails.application.routes.draw do
  get 'profile/show'

  get 'profile/edit'

  get 'social/index'
  get 'social/show_profile'
  patch 'social/update'

  get 'accounts/facebook'
  get 'accounts/instagram'
  get 'accounts/twitter'
  get 'accounts/google_oauth2'

  get 'auth/facebook/callback', to: 'accounts#facebook'
  get 'auth/twitter/callback', to: 'accounts#twitter'
  get 'auth/google_oauth2/callback', to: 'accounts#google_oauth2'
  get 'auth/instagram/callback', to: 'accounts#instagram'
  
  get 'auth/failure', to: redirect('/')

  devise_for :users, :controllers => { :omniauth_callbacks => "users/omniauth_callbacks" }
  resources :users, only: [:edit, :update]
  root 'social#index'
end
