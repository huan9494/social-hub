class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable
  has_many :accounts
  mount_uploader :user_avatar, FileUploader

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.user_name = auth.info.name
      user.remote_user_avatar_url = auth.info.image
    end
  end

  def self.get_data(auth, current_user)
    current_user.accounts.where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.user_email = auth.info.email
      user.user_name = auth.info.name
      user.user_avatar = auth.info.image
    end
  end
end
