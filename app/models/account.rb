class Account < ApplicationRecord
  belongs_to :user

  def self.auth_instagram(auth, current_user)
    current_user.accounts.create( provider: auth.provider, uid: auth.uid,
                                  oauth_token: auth.credentials.token,
                                  user_name: auth.info.name,
                                  user_email: auth.info.email,
                                  user_avatar: auth.info.image)
  end

  def self.auth_facebook(auth, current_user)
    current_user.accounts.create( provider: auth.provider, uid: auth.uid,
                                  oauth_token: auth.credentials.token,
                                  user_name: auth.info.name,
                                  user_email: auth.info.email,
                                  user_avatar: auth.info.image)
  end

  def self.auth_google_oauth2(auth, current_user)
    current_user.accounts.create( provider: auth.provider, uid: auth.uid,
                                  oauth_token: auth.credentials.token,
                                  user_name: auth.info.name,
                                  user_email: auth.info.email,
                                  user_avatar: auth.info.image,
                                  user_gender: auth.extra.raw_info.gender)
  end

  def self.auth_twitter(auth, current_user)
    current_user.accounts.create( provider: auth.provider, uid: auth.uid,
                                  oauth_token: auth.credentials.token,
                                  user_name: auth.info.name,
                                  user_email: auth.info.email,
                                  user_avatar: auth.info.image,
                                  user_address: auth.extra.raw_info.location)
  end
end
