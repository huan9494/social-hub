class UsersController < ApplicationController
  def edit
    @user = User.find(params[:id])
  end
  def update
    @user = User.find(params[:id])
    @user.update(user_params)
    redirect_to root_path
  end

  private
  def user_params
    params.require(:user).permit(:user_avatar, :user_name, :user_gender,
                                 :user_birthday, :user_address, :user_email)
  end
end