class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController

  def all
    if current_user
      @account = User.get_data(request.env["omniauth.auth"], current_user)
      redirect_to root_path
    else
      @account = Account.find_by_uid(request.env["omniauth.auth"].uid)
      if @account.present?
        @user = @account.user
        sign_in_and_redirect @user, :event => :authentication
      else
        @user = User.from_omniauth(request.env["omniauth.auth"])
        if @user.persisted?
          User.get_data(request.env["omniauth.auth"], @user)
          sign_in_and_redirect @user, :event => :authentication
        else
          session["devise.user_attributes"] = request.env["omniauth.auth"]
          redirect_to root_path
        end
      end
    end
  end

  def failure
    redirect_to root_path
  end

  alias_method :facebook, :all
  alias_method :twitter, :all
  alias_method :google_oauth2, :all
  alias_method :instagram, :all

end