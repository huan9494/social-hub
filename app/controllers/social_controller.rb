class SocialController < ApplicationController
  before_action :authenticate_user!
  def index
    @accounts = Account.all
    @account = current_user.accounts.find_by_provider(params[:provider])
  end
  def show_profile
    
  end
  def update
    current_user.update(
      remote_user_avatar_url: params[:avatar], user_name: params[:name],
      user_gender: params[:gender], user_birthday: params[:birthday],
      user_address: params[:address]
      )
    redirect_to root_path
  end
end
