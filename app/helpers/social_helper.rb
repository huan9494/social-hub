module SocialHelper
  def user_avatar
    if @account
      if @account.user_avatar
        @account.user_avatar
      else
        "NO DATA"
      end
    end
  end

  def user_address
    if @account
      if @account.user_address
        @account.user_address
      else
        "NO DATA"
      end
    end
  end

  def user_birthday
    if @account
      if @account.user_birthday
        @account.user_birthday
      else
        "NO DATA"
      end
    end
  end

  def user_gender
    if @account
      if @account.user_gender
        @account.user_gender
      else
        "NO DATA"
      end
    end
  end

  def user_name
    if @account
      if @account.user_name
        @account.user_name
      else
        "NO DATA"
      end
    end
  end

  def account_signed_in(account)
    if current_user.provider
      current_user.provider.downcase != account.downcase
    end
  end
end
