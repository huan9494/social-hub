class CreateAccounts < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :provider
      t.string :uid
      t.string :user_name
      t.string :oauth_token
      t.references :user, foreign_key: true
      t.string :user_gender
      t.string :user_address
      t.date :user_birthday
      t.string :user_avatar
      t.string :user_email

      t.timestamps
    end
  end
end
