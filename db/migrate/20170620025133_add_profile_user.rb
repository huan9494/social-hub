class AddProfileUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :user_avatar, :string
    add_column :users, :user_name, :string
    add_column :users, :user_gender, :string
    add_column :users, :user_birthday, :date
    add_column :users, :user_address, :string
  end
end
